import 'package:flutter/material.dart';
import 'package:sharedcollection/config/routes/app_router.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      theme: ThemeData(useMaterial3: true),
      routerConfig: appRouter,
      debugShowMaterialGrid: false,
    );
  }
}
