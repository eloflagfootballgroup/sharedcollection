<div align="center">
  <img src="https://i.imgur.com/XLjMZM0.png" width="600" alt="logo"/>
</div>
# sharedcollection

## 📋 Indice
  - [Descripcion](#descripcion)
  - [Features](#features)
  - [Wireframes](#wireframes)
## Descripcion

## Features

## Wireframes
<div align="center">
<a href="https://imgur.com/2yKtrWR"><img src="https://i.imgur.com/2yKtrWR.png" title="source: imgur.com" /></a> <a href="https://imgur.com/ZcxDJaJ"><img src="https://i.imgur.com/ZcxDJaJ.png" title="source: imgur.com" /></a> <a href="https://imgur.com/0e0jC3l"><img src="https://i.imgur.com/0e0jC3l.png" title="source: imgur.com" /></a> <a href="https://imgur.com/ftHF8Zk"><img src="https://i.imgur.com/ftHF8Zk.png" title="source: imgur.com" /></a> <a href="https://imgur.com/mrOJHaY"><img src="https://i.imgur.com/mrOJHaY.png" title="source: imgur.com" /></a> <a href="https://imgur.com/YqiOuSM"><img src="https://i.imgur.com/YqiOuSM.png" title="source: imgur.com" /></a> <a href="https://imgur.com/LiCCgSx"><img src="https://i.imgur.com/LiCCgSx.png" title="source: imgur.com" /></a>
</div>

## Estructura de capetas (feature first)
<div align="center">
<img src="https://blogs.brain-mentors.com/content/images/2022/04/FlutterAppArchitecure.001.jpeg" title="Estructura de capetas"/>
</div>